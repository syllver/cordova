/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */



// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready

document.addEventListener('deviceready', onDeviceReady, false);
var baseURL = 'https://fdss-eut.biotechfarms.net/fdss-api';
var db = null;
var conn;
async function onDeviceReady() {
    // Cordova is now initialized. Have fun!

    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    document.getElementById('deviceready').classList.add('ready');

    await checkConnection()
    await initDatabase()
    await window.sqlitePlugin.echoTest(function() {
        // console.log('ECHO test OK');
        document.addEventListener("online", onOnline, false);
        document.addEventListener("offline", onOffline, false);
        // console.log("33333");
    });
    // await onOnline();
    syncPrompt();
}

var networkConnection = "";

function checkConnection() {
    var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';
    conn = states[networkState];
    document.getElementById("netStatus").innerText = "YOU ARE CONNECTED TO \n" + states[networkState];
    // alert('Connection type: ' + states[networkState]);
}

async function query(sql, parameter, ) {
    return await new Promise(function(resolve) {
        db.transaction(function(transaction) {
            transaction.executeSql(sql, parameter, function(err, resultSet) {
                let data = []
                if(resultSet.rows.length > 0 ){
                    resolve(resultSet);
                } else if(resultSet.rows.length == 0 ){
                    resolve(null);
                } else {
                    reject(err);
                }
            })
        })
    })
}

function syncPrompt() {
    var modal = new bootstrap.Modal(document.getElementById("syncModal"));
    modal.show();
    
    // var answer = confirm("YOU ARE CONNECTED TO "+conn)
    // if(answer) {
    //     onOnline
    // } else {

    // }
}

async function onOnline() {
    // alert("online!")
    var loader = new bootstrap.Modal(document.getElementById("loader"));
    networkConnection = 'online'
    var tables = ['demo'];
    var lastSync;

    // get last sync date
    await db.transaction(async function(transaction) {
        // check date of last sync from replication tables
        await transaction.executeSql("SELECT * FROM replication WHERE userId = ? ORDER BY lastsync DESC LIMIT 1", [8696], async function(err, resultSet) {
            let allLocation = await selectAllFromDemo();
            if(resultSet.rows.length > 0) {
                lastSync = resultSet.rows.item(0);
                console.log("lastSync", JSON.stringify(lastSync));
                    // if last sync date found
                    // get all data saved offline after the last sync date
                    if(allLocation != null) {
                        let dataAfterSync = [];
                        for(var i = 0; i < allLocation.rows.length; i++) {
                            if(allLocation.rows.item(i).U_CREATED_AT > lastSync.lastsync) {
                                dataAfterSync.push(allLocation.rows.item(i));
                            }
                        }

                        // if found offline data after last sync date 
                        // upload
                        if(dataAfterSync) {
                            // sync data online
                            await syncDataOnline(dataAfterSync)
                        }
                    } else {
                        // if no offline data found
                        // sync all online data to offline
                        console.log("STOP!");
                    }

                    console.log("after login");

                    
            } else {
                // if date of last sync is not found 
                // need to sync online and offline
                console.log("last sync data not found");
                let dataToSync = []

                // console.log(JSON.stringify(allLocation));
                if(allLocation != null) {
                    console.log(JSON.stringify(allLocation));
                    for(var i = 0; i < allLocation.rows.length; i++ ) {
                        dataToSync.push(allLocation.rows.item(i));
                    }
                }

                // sync online
                await syncDataOnline(dataToSync);

                // sync offline
                console.log("STOP");
                // await syncDataOffline(null)
            }
        }, function () {
            // loader.hide();
            alert("Data synced")
            console.log("done");
        })
    }, function (error) {
        document.getElementById("syncStatus").innerText = "Error in sync: ", error;
    }, function () {
        // alert("Data synced")
        console.log("done onOnline");
    })
}

function onOffline() {
    // Handle the offline event
    networkConnection = "offline";
}

async function initDatabase() {
    console.log("AAAAAAAA");
    db = await window.sqlitePlugin.openDatabase({
        name: 'samplecrud',
        location: 'default'
    }, function(db) {
        // db.transaction(function(transaction) {
        //     transaction.executeSql('DROP TABLE locations');
        // })
        db.transaction(function(transaction) {
            transaction.executeSql(`CREATE TABLE IF NOT EXISTS locations (
                Id INTEGER PRIMARY KEY AUTOINCREMENT,
                Code INTEGER,
                U_ADDRESS TEXT NOT NULL,
                U_CREATED_AT DATE DEFAULT (datetime('now','localtime')) NOT NULL,
                U_UPDATED_AT DATE DEFAULT (datetime('now','localtime'))
            )`);
        }, function (err) {
            console.log(err);
        });

        db.transaction(function(transaction) {
            transaction.executeSql(`CREATE TABLE IF NOT EXISTS replication (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                userId int NOT NULL,
                lastsync DATE DEFAULT (datetime('now','localtime')) NOT NULL
            )`)
        })

        // db.transaction(function(transaction) {
        //     transaction.executeSql(`INSERT INTO replication (userId) VALUES (?)`, ['4512'])
        // }, function(error) {
        //     alert("Insert into replication ERROR: ", error.message);
        // }, function() {
        //     alert("Success insert into replication")
        // })
    }, function(err) {
        console.log("Open database ERROR: "+ JSON.stringify(err));
        alert("Open database ERROR: "+ JSON.stringify(err));
    });
    getInfo();
    return;
}

async function insertInfo(data) {
    let successfulInsert = [];
    if((Array.isArray(data) || ((typeof data === "object" || typeof data === 'function') && (data !== null))) && data != null) {
        if(data.U_CREATED_AT || data.U_UPDATED_AT) {
            console.log(JSON.stringify(data));
            var U_ADDRESS = (data.U_ADDRESS);
            var U_CREATED_AT = (data.U_CREATED_AT);
            var U_UPDATED_AT = (data.U_UPDATED_AT);
            await db.transaction(async function(transaction) {
                await transaction.executeSql('INSERT INTO locations(U_ADDRESS, U_CREATED_AT, U_UPDATED_AT) VALUES (?, ?, ?)', [U_ADDRESS, U_CREATED_AT, U_UPDATED_AT]);
            }, function (error) {
                successfulInsert.push({
                    status: 0,
                    response: error,
                    data: U_ADDRESS
                })
            }, function() {
                successfulInsert.push({
                    status: 1,
                    response: "Success",
                    data: U_ADDRESS
                })
            })
        } else {
            console.log(JSON.stringify(data));
            var U_ADDRESS = (data.U_ADDRESS);
            await db.transaction(async function(transaction) {
                await transaction.executeSql('INSERT INTO locations(U_ADDRESS) VALUES (?)', [U_ADDRESS]);
            }, function (error) {
                successfulInsert.push({
                    status: 0,
                    response: error,
                    data: U_ADDRESS
                })
            }, function() {
                successfulInsert.push({
                    status: 1,
                    response: "Success",
                    data: U_ADDRESS
                })
            })
        }
    } else {
        console.log("DATA", JSON.stringify(data));

        await db.transaction(async function(transaction) {
            await transaction.executeSql('INSERT INTO locations(U_ADDRESS) VALUES (?)', [data]);
        }, function (error) {
            successfulInsert.push({
                status: 0,
                response: error,
                data: data[i].U_ADDRESS
            })
        }, function() {
            successfulInsert.push({
                status: 1,
                response: "Success",
                data: data[i].U_ADDRESS
            })
        })
    }

    await getInfo();
    console.log("DONE",successfulInsert);
    return successfulInsert;
}

async function replicateOffline(data) {
    var Code = (data.Code)
    console.log(JSON.stringify(data));
    var U_ADDRESS = (data.U_ADDRESS);
    var U_CREATED_AT = (data.U_CREATED_AT);
    var U_UPDATED_AT = (data.U_UPDATED_AT);
    await db.transaction(async function(transaction) {
        await transaction.executeSql('INSERT INTO locations(Code, U_ADDRESS, U_CREATED_AT, U_UPDATED_AT) VALUES (?, ?, ?, ?)', [Code, U_ADDRESS, U_CREATED_AT, U_UPDATED_AT]);
    }, function (error) {
        successfulInsert.push({
            status: 0,
            response: error,
            data: U_ADDRESS
        })
    }, function() {
        successfulInsert.push({
            status: 1,
            response: "Success",
            data: U_ADDRESS
        })
    })
}

async function getInfo() {
    let data = [];
    let res = await selectAllFromDemo();
    if(res) {
        for(var i = 0; i < res.rows.length; i++) {
            data.push({
                Code: res.rows.item(i).Code,
                Name: res.rows.item(i).Name, 
                U_ADDRESS: res.rows.item(i).U_ADDRESS,
                U_CREATED_AT: res.rows.item(i).U_CREATED_AT,
                U_UPDATED_AT: res.rows.item(i).U_UPDATED_AT,
            })
        }
    }
    showData(data)
}

async function getInfoById(id) {
    return await db.transaction(function(transaction) {
        return transaction.executeSql("SELECT * FROM locations WHERE Code = ?", [id], function(ignored, resultSet) {
            document.getElementById("upd_address").value = resultSet.rows.item(0).U_ADDRESS;
            document.getElementById("upd_code").value = resultSet.rows.item(0).Code;
        })
    }, function (error) {
        alert("Error get info by Code: ", error.message);
    })
}

function updateInfo() {
    var address = document.getElementById("upd_address").value;
    var code = document.getElementById("upd_code").value;
    
    db.transaction(function(transaction) {
        transaction.executeSql("UPDATE locations SET U_ADDRESS = ? WHERE Code = ?", [firstname, age, id])
    }, function (error) {
        alert('Update ERROR: '+ error.message)
    }, function() {
        alert("Successfully U_UPDATED_AT");
        getInfo();
        document.getElementById("upd_address").value = "";
        document.getElementById("upd_code").value = "";
    })
}

function deleteInfo(id) {
    db.transaction(function(transaction) {
        transaction.executeSql("DELETE FROM locations WHERE Code = ?", [id])
    }, function (error) {
        alert("DELETE error: ", error.message);
    }, function() {
        alert("Successfully deleted")
        getInfo();
    })
}

///////////////////////////// show data ////////////////////////////////
async function submitMe() {
    var address =  document.getElementById("address").value;
    console.log("address:", address);

    if(address != "") {
        await insertInfo(address);
        document.getElementById("address").value = ""
        document.getElementById("address").innerText = ""
    }
}

function showData(arr) {
    var table = document.getElementById("table");
    table.innerHTML = '';

    for(var i = 0; i < arr.length; i++) {  
        var row = table.insertRow(0);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        cell1.innerText = arr[i].Code;
        cell2.innerText = arr[i].U_ADDRESS;
        // cell3.innerText = new Date(arr[i].U_CREATED_AT).toLocaleDateString('en-GB', {
        //     day : 'numeric',
        //     month : 'short',
        //     year : 'numeric',
        //     hour12: false,
        //     hour: '2-digit',
        //     minute: '2-digit'
        // });
        // cell4.innerHTML = `<button class="btn btn-sm btn-primary" 
        //     onclick="updateMe(${arr[i].id})"
        // >Edit</button>`;
        cell3.innerHTML = `<button class="btn btn-sm btn-danger" 
            onclick="deleteInfo(${arr[i].Code})"
        >Delete</button>`;
    }
}

async function updateMe(id) {
    await getInfoById(id).then(res => {
        var updateModal = new bootstrap.Modal(document.getElementById('updateModal'));
        updateModal.show();
    })
}

function cancelUpdate() {
    document.getElementById("upd_address").value = "";
    document.getElementById("upd_code").value = "";
}

///////////////////////////// replication //////////////////////////////
async function selectAllFromDemo() {
    return await new Promise(function(resolve) {
        db.transaction(function(transaction) {
            transaction.executeSql("SELECT * FROM locations ORDER BY Code DESC", [], function(err, resultSet) {
                // let data = []
                console.log(JSON.stringify(resultSet));
                if(resultSet.rows.length > 0 ){
                    resolve(resultSet);
                } else if(resultSet.rows.length == 0 ){
                    resolve(null);
                } else {
                    reject(err);
                }
            })
        })
    })
}

/////////////////////// sync data online ///////////////////////////////
async function syncDataOnline(dataToSync) {
    let sessionId;
    let successfulInsert = [];
    
    var dataNotFoundOnline = [];
    await $.get(`${baseURL}/api/location/select`, async function(data) {
        // if found data, compare with offline
        for(var i = 0; i < dataToSync.length; i++) {
            // search offline in online data
            var match = false; // flag for not found data
            for(var j = 0; j < data.view.length; j++) {
                if(dataToSync[i].U_ADDRESS == data.view[j].U_ADDRESS) {
                    // found offline data in online
                    match = true;
                    break;
                }
                // if data offline is not found in data online, loop will end
                // match is still false
            }
            // if match is true, add offline in new arr
            if(!match) {
                dataNotFoundOnline.push(dataToSync[i])
            }
        }
    })

    console.log("dataNotFoundOnline", JSON.stringify(dataNotFoundOnline));

    
    if(dataNotFoundOnline.length > 0) {
        // login to fdss
        $.post( `${baseURL}/login`, {
            username: "admin", password: "1234"
        }).done(async function(data) {
            // store sessionId and add location
            sessionId = data.SessionId;
            
            for(var i = 0; i < dataNotFoundOnline.length; i++) {
                await $.ajax({
                    type: "POST",
                    url: `${baseURL}/api/location/add`,
                    data: { 
                        U_ADDRESS: dataNotFoundOnline[i].U_ADDRESS,
                        U_CREATED_AT: dataNotFoundOnline[i].U_CREATED_AT
                    },
                    headers: {
                        Authorization: `B1SESSION=${sessionId}`
                    },
                    dataType: 'json',
                    success: function(data) {
                        console.log("SUCCESSFUL INSERT: ", JSON.stringify(data));
                        successfulInsert.push({
                            status: 1,
                            response: data,
                            data: dataNotFoundOnline[i]
                        })
                    },
                    error: function(err) {
                        console.log("ERROR ADDING NEW LOCATION: ", JSON.stringify(err));
                        successfulInsert.push({
                            status: 0,
                            response: err,
                            data: dataNotFoundOnline[i]
                        })
                    }
                })
            }

            // await getSyncedData(dataNotFoundOnline);

        }).fail(function(err) {
            console.log("LOGIN ERROR: ", err);
        }).always(function() {
            console.log(JSON.stringify(successfulInsert));
        })
    }
    await getSyncedData(dataNotFoundOnline);
    
}

async function syncDataOffline(lastSync) {
    var dataOffline = await selectAllFromDemo();
    var dataNotFoundOffline = [];

    lastSync != null ? (
        console.log("lastSync syncDataOffline", lastSync)
    ) : (
        console.log("nothing")
    )

    await $.get(`${baseURL}/api/location/select`, async function(data) {
        // if found data, compare with offline
        if(dataOffline.length > 0) { // if there is data offline to be compared with
            for(var i = 0; i < data.view.length; i++) {
                // we want to know if data is found in offline
                var match = false; // flag for not found data
                for(var j = 0; j < dataOffline.rows.length; j++) {
                    if(data.view[i].U_ADDRESS == dataOffline.rows.item(j).U_ADDRESS) {
                        // found online data in offline
                        match = true;
                        break;
                    }
                    // if data online is not found in data offline, loop will end
                    // match is still false
                }
                // if match is true, add online in new arr
                if(!match) {
                    dataNotFoundOffline.push(data.view[i])
                }
            }
            
            // console.log("dataNotFoundOffline", JSON.stringify(dataNotFoundOffline));
            // get data not found online
            if(lastSync != null) {
                var temp = [];
                for(var i = 0; i < dataNotFoundOffline.length; i++) {
                    if(dataNotFoundOffline[i].U_CREATED_AT > lastSync.lastsync) {
                        temp.push(dataNotFoundOffline[i])
                    }
                }
                dataNotFoundOffline = temp;
            }

            if(dataNotFoundOffline.length > 0) {
                for(var i = 0; dataNotFoundOffline.length; i++) {
                    if(dataNotFoundOffline[i] != null) {
                        console.log("not found offline: ", JSON.stringify(dataNotFoundOffline[i].U_ADDRESS));   
                        await insertInfo(dataNotFoundOffline[i])
                    }
                }
            }
        } else {
            // if no data offline
            // direct insert to offline, no comparison
            for(var i = 0; dataNotFoundOffline.length; i++) {
                if(dataNotFoundOffline[i] != null) {
                    console.log("not found offline: ", JSON.stringify(dataNotFoundOffline[i].U_ADDRESS));   
                    await insertInfo(dataNotFoundOffline[i])
                }
            }
        }
    }).done(async function() {
        console.log("Online data downloaded");
    }).fail(function() {
        alert( "error" );
    }).always(function() {
        console.log("Finished online data download");
    });
    

    // await $.get(`${baseURL}/api/location/select`, async function(data) {
    //     // if found data, compare with offline
    //     if(dataOffline.length > 0) {
    //         for(var i = 0; i < data.view.length; i++) {
    //             // we want to know if data is found in offline
    //             var match = false; // flag for not found data
    //             for(var j = 0; j < dataOffline.rows.length; j++) {
    //                 if(data.view[i].U_ADDRESS == dataOffline.rows.item(j).U_ADDRESS) {
    //                     // found online data in offline
    //                     match = true;
    //                     break;
    //                 }
    //                 // if data online is not found in data offline, loop will end
    //                 // match is still false
    //             }
    //             // if match is true, add online in new arr
    //             if(!match) {
    //                 dataNotFoundOffline.push(data.view[i])
    //             }
    //         }
            
    //         // console.log("dataNotFoundOffline", JSON.stringify(dataNotFoundOffline));
    //         if(dataNotFoundOffline.length > 0) {
    //             for(var i = 0; dataNotFoundOffline.length; i++) {
    //                 if(dataNotFoundOffline[i] != null) {
    //                     console.log("not found offline: ", JSON.stringify(dataNotFoundOffline[i].U_ADDRESS));   
    //                     await insertInfo(dataNotFoundOffline[i])
    //                 }
    //             }
    //         }
    //     } else {
    //         for(var i = 0; dataNotFoundOffline.length; i++) {
    //             if(dataNotFoundOffline[i] != null) {
    //                 console.log("not found offline: ", JSON.stringify(dataNotFoundOffline[i].U_ADDRESS));   
    //                 await insertInfo(dataNotFoundOffline[i])
    //             }
    //         }
    //     }
    // }).done(async function() {
    //     console.log("Online data downloaded");
    // }).fail(function() {
    //     alert( "error" );
    // }).always(function() {
    //     console.log("Finished online data download");
    // });
}

async function checkIfDataExists(data1, data2) {
    let arr = [];

    for(var i = 0; i < data1.length; i++) {
        for(var j = 0; j < data2.length; j++) {
            if(data1[i].U_ADDRESS != data2[j].U_ADDRESS) {
                arr.push(data2[j]);
            }
        }
    }
}

async function getSyncedData(dataSynced) {
    console.log("getSynced to delete", JSON.stringify(dataSynced));
    let toReplicateOffline = []; 
    await $.get(`${baseURL}/api/location/select`, async function(data) {
        console.log("ASASAS", JSON.stringify(data.view));
        if(dataSynced.length > 0) {
            for(var i = 0; i < dataSynced.length; i++) {
                var match = false;
                for(var j = 0; j < data.view.length; j++) {
                    console.log("/////////////////////////////////////");
                    console.log(dataSynced[i].U_ADDRESS, "><><><><", data.view[j].U_ADDRESS);
                    if(dataSynced[i].U_ADDRESS == data.view[j].U_ADDRESS) {
                        match = true;
                        break;
                    }
                }

                if(!match) {
                    toReplicateOffline.push(data.view[i]);
                }

                // console.log("data.view", data.view[i].U_ADDRESS);
                // console.log("dataSynced", dataSynced[i].U_ADDRESS);
                // if(data.view[i].U_ADDRESS == dataSynced[i].U_ADDRESS) {
                //     toReplicateOffline.push(data.view[i]);
                // }
            }
    
            // delete what is stored offline
            for(var i = 0; i < toReplicateOffline.length; i++) {
                await deleteInfo(toReplicateOffline[i].Code)
            }
    
            // replicate data synced online
            // in order for same db, online and offline, to have same code 
            for(var i = 0; i < toReplicateOffline.length; i++) {
                replicateOffline(toReplicateOffline[i]);
            }
        } else {
            console.log("datasynced no length");
        }
        
    }).done(async function() {
        console.log("Online data downloaded");
    }).fail(function() {
        alert( "error" );
    }).always(function() {
        console.log("Finished online data download");
    });
}