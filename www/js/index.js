/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */



// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready

document.addEventListener('deviceready', onDeviceReady, false);

var db = null;
async function onDeviceReady() {
    // Cordova is now initialized. Have fun!

    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    document.getElementById('deviceready').classList.add('ready');

    await checkConnection()
    await initDatabase()
    await window.sqlitePlugin.echoTest(function() {
        // console.log('ECHO test OK');
        document.addEventListener("online", onOnline, false);
        document.addEventListener("offline", onOffline, false);
        // console.log("33333");
    });

    // var xhr = new XMLHttpRequest();
    //     xhr.open('GET', 'https://reqres.in/api/users/2'), 
    //     xhr.responseType = 'json';
    //     xhr.send();
    //     console.log("AAAAA");
    //     xhr.onprogress = function() {
    //         console.log('LOADING', xhr.status);
    //     }
    //     xhr.onload = function(e) {
    //         console.log("HELLOW");
    //             // this will be called after the response is received
    //             if(xhr.status != 200) {
    //                 console.log(`Error ${xhr.status}: ${xhr.statusText}`);
    //             } else {
    //                 console.log("SASASASAS", JSON.stringify(xhr.response));
    //                 console.log(`Result: ${xhr.response}`);
    //             }
    //     }

      

          

        // var login = {
            //     "name": "kharyl",
            //     "job": "ux"
            // }

            // var jqxhr = $.post( "https://reqres.in/api/users", login)
            //     .done(function( data ) {
            //         console.log("data loaded: ", JSON.stringify(data));
            //     })

            // console.log("DONEEEEE: ", JSON.stringify(jqxhr));
        
        // var jqxhr = $.get( "https://reqres.in/api/users/2", function(data) {
        //     alert( "success" );
        //     console.log("data", JSON.stringify(data));
        //     })
        //     .done(function() {
        //         alert( "second success" );
        //     })
        //     .fail(function() {
        //         alert( "error" );
        //     })
        //     .always(function() {
        //         alert( "finished" );
        //     });
}

var networkConnection = "";

function checkConnection() {
    var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';
    alert('Connection type: ' + states[networkState]);
}

async function query(sql, parameter, ) {
    return await new Promise(function(resolve) {
        db.transaction(function(transaction) {
            transaction.executeSql(sql, parameter, function(err, resultSet) {
                let data = []
                if(resultSet.rows.length > 0 ){
                    resolve(resultSet);
                } else if(resultSet.rows.length == 0 ){
                    resolve(null);
                } else {
                    reject(err);
                }
            })
        })
    })
}

async function onOnline() {
    alert("online!")
    networkConnection = 'online'
    var tables = ['demo'];
    let data;

// get last sync date
    let promise = await query("SELECT * FROM replication where 'userId' = ?", ['8696']);
    if(promise) {
        promise.then( res => {
            for(var i = 0; i < res.rows.length; i++) {
                data.push({
                    id: res.rows.item(i).id,
                    userId: res.rows.item(i).userId,
                    lastsync: res.rows.item(i).lastsync,
                })
            }
        
        }).catch(err => console.log("error: ",err))
    }

    if(!data) {
        // start replicating
        let local_demo = [];
        let local = await selectAllFromDemo();

        for(var i = 0; i < local.rows.length; i++) {
            local_demo.push({
                id: local.rows.item(i).id,
                name: local.rows.item(i).name,
                date_created: local.rows.item(i).date_created,
                date_updated: local.rows.item(i).date_updated,
            })
        }

        console.log("local_demo", local_demo);

        

        // var xhr = new XMLHttpRequest();
        // xhr.open('GET', 'http://localhost:3000/users'), 
        // xhr.responseType = 'json';
        // xhr.send();
        // console.log("AAAAA");
        // xhr.onprogress = function() {
        //     console.log('LOADING', xhr.status);
        // }
        // xhr.onload = function(e) {
        //     console.log("HELLOW");
        //         // this will be called after the response is received
        //         if(xhr.status != 200) {
        //             console.log(`Error ${xhr.status}: ${xhr.statusText}`);
        //         } else {
        //             console.log(`Result: ${xhr.response}`);
        //         }
        // }

        // console.log("HELLO");
    } else {
        console.log("do nothing. yet");
        // query those that need to be replicated
    }
}

function onOffline() {
    // Handle the offline event
    networkConnection = "offline";
}

async function initDatabase() {
    db = await window.sqlitePlugin.openDatabase({
        name: 'sample.db',
        location: 'default'
    }, function(db) {
        // db.transaction(function(transaction) {
        //     transaction.executeSql('DROP TABLE replication');
        // })
        db.transaction(function(transaction) {
            transaction.executeSql(`CREATE TABLE IF NOT EXISTS demo (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                name TEXT NOT NULL,
                age INT NOT NULL,
                date_created DATE DEFAULT (datetime('now','localtime')) NOT NULL,
                date_updated DATE DEFAULT (datetime('now','localtime'))
            )`);
        });

        db.transaction(function(transaction) {
            transaction.executeSql(`CREATE TABLE IF NOT EXISTS replication (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                userId int NOT NULL,
                lastsync DATE DEFAULT (datetime('now','localtime')) NOT NULL
            )`)
        })
        // db.transaction(function(transaction) {
        //     transaction.executeSql(`INSERT INTO replication (userId) VALUES (?)`, ['4512'])
        // }, function(error) {
        //     alert("Insert into replication ERROR: ", error.message);
        // }, function() {
        //     alert("Success insert into replication")
        // })
    }, function(err) {
        console.log("Open database ERROR: "+ JSON.stringify(err));
        alert("Open database ERROR: "+ JSON.stringify(err));
    });
    // console.log("22222");
    getInfo();
    return;
}

async function insertInfo() {
    // var date = new moment().format();
    var firstname = document.getElementById("firstname").value;
    var age = document.getElementById("age").value;
    await db.transaction(function(transaction) {
        transaction.executeSql('INSERT INTO demo(name, age) VALUES (?, ?)', [firstname, age]);
    }, function (error) {
        console.log("Transaction ERROR: "+ error.message);
        alert("Transaction ERROR: "+ error.message);
    }, function() {
        console.log("Populated database OK");
        alert("Success");
        getInfo();
    })
}

async function getInfo() {
    let data = [];
    let res = await selectAllFromDemo();
    if(res) {
        for(var i = 0; i < res.rows.length; i++) {
            data.push({
                id: res.rows.item(i).id,
                name: res.rows.item(i).name, 
                age: res.rows.item(i).age,
                date_created: res.rows.item(i).date_created,
            })
        }
    }
    showData(data)
}

async function getInfoById(id) {
    return await db.transaction(function(transaction) {
        return transaction.executeSql("SELECT * FROM demo WHERE id = ?", [id], function(ignored, resultSet) {
            // alert((resultSet.rows.item(0)));
            // console.log(resultSet.rows.item(0).id);
            // return resultSet.rows.item(0)
            // for(var i = 0; i < resultSet.rows.length; i++) {
            // return "hello";
            // var obj = {
                //     id: JSON.stringify(resultSet.rows.item(0).id),
                //     name: JSON.stringify(resultSet.rows.item(0).name), 
                //     age: JSON.stringify(resultSet.rows.item(0).age)
                // }
            
                // return JSON.stringify(obj)
                // data.push(obj)
                // alert("OBJ", JSON.stringify(obj))
            

            // return data;
            // return resultSet.rows.item(0);
            // alert(resultSet)
            document.getElementById("upd_firstname").value = resultSet.rows.item(0).name;
            document.getElementById("upd_age").value = resultSet.rows.item(0).age;
            document.getElementById("upd_id").value = resultSet.rows.item(0).id;
            // document.getElementById("btn-update").style.display = 'block';
            // document.getElementById("btn-submit").style.display = 'none';
        })
    }, function (error) {
        alert("Error get info by id: ", error.message);
    })
}

function updateInfo() {
    var firstname = document.getElementById("upd_firstname").value;
    var age = document.getElementById("upd_age").value;
    var id = document.getElementById("upd_id").value;
    
    db.transaction(function(transaction) {
        transaction.executeSql("UPDATE demo SET name = ?, age = ? WHERE id = ?", [firstname, age, id])
    }, function (error) {
        alert('Update ERROR: '+ error.message)
    }, function() {
        alert("Successfully updated");
        getInfo();
        document.getElementById("upd_firstname").value = "";
        document.getElementById("upd_age").value = "";
        document.getElementById("upd_id").value = "";
    })
}

function deleteInfo(id) {
    db.transaction(function(transaction) {
        transaction.executeSql("DELETE FROM demo WHERE id = ?", [id])
    }, function (error) {
        alert("DELETE error: ", error.message);
    }, function() {
        alert("Successfully deleted")
        getInfo();
    })
}


///////////////////////////// replication //////////////////////////////
async function selectAllFromDemo() {
    return await new Promise(function(resolve) {
        db.transaction(function(transaction) {
            transaction.executeSql("SELECT * FROM demo ORDER BY id DESC", [], function(err, resultSet) {
                // let data = []
                console.log(JSON.stringify(resultSet));
                if(resultSet.rows.length > 0 ){
                    resolve(resultSet);
                } else if(resultSet.rows.length == 0 ){
                    resolve(null);
                } else {
                    reject(err);
                }
            })
        })
    })
}