/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */



// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready

document.addEventListener('deviceready', onDeviceReady, false);
var baseURL = 'https://fdss-eut.biotechfarms.net/fdss-api';
var db = null;
var conn;
async function onDeviceReady() {
    // Cordova is now initialized. Have fun!

    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    document.getElementById('deviceready').classList.add('ready');

    await checkConnection()
    await initDatabase()
    await window.sqlitePlugin.echoTest(function() {
        document.addEventListener("online", function() {
            networkConnection = 'online'
        }, false);
        document.addEventListener("offline", function() {
            networkConnection = 'offline'
        }, false);
    });
}

var networkConnection;
var connectionError;

async function checkConnection() {
    var networkState = navigator.connection.type;
    var states = {};

    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';
    conn = states[networkState];

    console.log("checking connection...");
    if(navigator.onLine) {
        networkConnection = 'offline';
        console.log("aaaa");
        await $.post( `${baseURL}/login`, {
            username: "admin", password: "1234"
        }).done(function(data) {
            networkConnection = "online";
            console.log("network connection: online");
        }).fail(function(err){
            connectionError = err.message;
        }).always(function() {
            console.log("connection checked");
        })
    } else {
        console.log("offline");
        networkConnection = "offline";
        console.log("network connection: offline");
    }

    
    console.log("conn: ", conn);
    document.getElementById("netStatus").innerText = "YOU ARE CONNECTED TO \n" + states[networkState];
    console.log("=======================================");
    console.log("networkConnection", networkConnection);
}

async function query(sql, parameter, ) {
    return await new Promise(function(resolve) {
        db.transaction(function(transaction) {
            transaction.executeSql(sql, parameter, function(err, resultSet) {
                let data = []
                if(resultSet.rows.length > 0 ){
                    resolve(resultSet);
                } else if(resultSet.rows.length == 0 ){
                    resolve(null);
                } else {
                    reject(err);
                }
            })
        })
    })
}

async function initDatabase() {
    db = await window.sqlitePlugin.openDatabase({
        name: 'samplecrud',
        location: 'default'
    }, function(db) {
        // db.transaction(function(transaction) {
        //     transaction.executeSql('DROP TABLE sampletable');
        // })
        db.transaction(function(transaction) {
            transaction.executeSql(`CREATE TABLE IF NOT EXISTS sampletable (
                Id INTEGER PRIMARY KEY AUTOINCREMENT,
                Code INTEGER,
                U_ADDRESS TEXT NOT NULL UNIQUE,
                U_CREATED_AT DATE DEFAULT (datetime('now','localtime')),
                U_UPDATED_AT DATE DEFAULT (datetime('now','localtime'))
            )`);
        }, function (err) {
            console.log(err);
        });

        // db.transaction(function(transaction) {
        //     transaction.executeSql(`CREATE TABLE IF NOT EXISTS replication (
        //         id INTEGER PRIMARY KEY AUTOINCREMENT,
        //         userId int NOT NULL,
        //         lastsync DATE DEFAULT (datetime('now','localtime')) NOT NULL
        //     )`)
        // })

        // db.transaction(function(transaction) {
        //     transaction.executeSql(`INSERT INTO replication (userId) VALUES (?)`, ['4512'])
        // }, function(error) {
        //     alert("Insert into replication ERROR: ", error.message);
        // }, function() {
        //     alert("Success insert into replication")
        // })
    }, function(err) {
        console.log("Open database ERROR: "+ JSON.stringify(err));
        alert("Open database ERROR: "+ JSON.stringify(err));
    });
    showLocations();
    return;
}

async function onOnline() {
    if(window.navigator.onLine) {
        networkConnection = 'online'
        var modal = new bootstrap.Modal(document.getElementById("syncModal"));
        modal.show();
    } else {
        networkConnection = 'offline'
        var modal = new bootstrap.Modal(document.getElementById("syncModalOffline"));
        modal.show();
    }
}

async function onOffline() {
    networkConnection = 'offline';
}

async function getLocations() {
    // console.log("select from offline");
    return await new Promise(function(resolve) {
        db.transaction(function(transaction) {
            transaction.executeSql("SELECT * FROM sampletable ORDER BY Id ASC", [], function(err, resultSet) {
                // console.log("temp:", JSON.stringify(resultSet));
                if(resultSet.rows.length > 0 ){
                    resolve(resultSet);
                } else if(resultSet.rows.length == 0 ){
                    resolve(null);
                } else {
                    reject(err);
                }
            })
        })
    })
}

async function showLocations() {
    // console.log("showing location");
    var table = document.getElementById("table");
    table.innerHTML = '';
    let data = [];

    let re = await getLocations();
    console.log(JSON.stringify(re));
    if(re) {
        for(var i = 0; i < re.rows.length; i++) {
            data.push({
                Id: re.rows.item(i).Id,
                Code: re.rows.item(i).Code ? re.rows.item(i).Code : "",
                U_ADDRESS: re.rows.item(i).U_ADDRESS,
                U_CREATED_AT: re.rows.item(i).U_CREATED_AT,
                U_UPDATED_AT: re.rows.item(i).U_UPDATED_AT,
            })
        }
        console.log(JSON.stringify(data));

        for(var i = 0; i < data.length; i++) { 
            var row = table.insertRow(-1);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            cell1.innerText = data[i].Id;
            cell2.innerText = data[i].U_ADDRESS;
            // cell3.innerText = new Date(arr[i].U_CREATED_AT).toLocaleDateString('en-GB', {
            //     day : 'numeric',
            //     month : 'short',
            //     year : 'numeric',
            //     hour12: false,
            //     hour: '2-digit',
            //     minute: '2-digit'
            // });
    
            cell3.innerHTML = `<button class="btn btn-sm btn-primary rounded-circle" id="btn-update"
                 onclick="setUpdate(${ data[i].Id })">
                 <span class="fa-solid fa-pencil"></span></button>`
            //      <button class="btn btn-sm btn-danger rounded-circle" 
            //     onclick="deleteInfo(${ data[i].Id })"
            // ><span class="fa-solid fa-trash"></span></button>`
        }
    }
}

async function addLocation(address) {
    var address =  document.getElementById("address").value;

    if(networkConnection == 'online') {
        console.log("add online");
        addOnline(address)
    }

    console.log("address", address);

    return await db.transaction(async function(transaction) {
        await transaction.executeSql(`INSERT INTO sampletable(U_ADDRESS, U_CREATED_AT) VALUES (?, ?)`, [address, moment().format()]);
    }, async function (error) {
        console.log("ERROR INSERT ", error);
    }, async function() {
        document.getElementById("address").value = "";
        alert("Success")
        await showLocations();
        console.log("SUCCESSFUL INSERT");
    })
    
}

async function addOnline(address) {
    // console.log("function add online");
    $.post( `${baseURL}/login`, {
        username: "admin", password: "1234"
    }).done(async function(data) {
        // store sessionId and add location
        var session = data.SessionId;

        await $.ajax({
            type: "POST",
            url: `${baseURL}/api/location/add`,
            data: { 
                U_ADDRESS: address,
                U_CREATED_AT: moment().format()
            },
            headers: {
                Authorization: `B1SESSION=${session}`
            },
            dataType: 'json',
            success: function() {
                console.log("Successfully added");
            },
            error: function(err) {
                console.log("Error adding new location online: ", JSON.stringify(err));
            }
        })
    }).fail(function(err){
    }).always(function() {
    })
}

async function setUpdate(id) {
    var data = await getLocationById(id);
    var modal = new bootstrap.Modal(document.getElementById("updateModal"));
    if(data) {
        document.getElementById("upd_id").value = data.rows.item(0).Id
        document.getElementById("upd_address").value = data.rows.item(0).U_ADDRESS
        data.rows.item(0).Code ? 
            document.getElementById("upd_code").value = data.rows.item(0).Code : '';
        modal.show();
    }
}

async function getLocationById(id) {
    return await new Promise(function(resolve) {
        db.transaction(function(transaction) {
            transaction.executeSql("SELECT * FROM sampletable WHERE Id = ?", [id], function(err, resultSet) {
                if(resultSet.rows.length > 0 ){
                    resolve(resultSet);
                } else if(resultSet.rows.length == 0 ){
                    resolve(null);
                } else {
                    reject(err);
                }
            })
        })
    })
}

async function updateLocation() {
    var id = document.getElementById("upd_id").value;
    var code = document.getElementById("upd_code").value;
    var address = document.getElementById("upd_address").value;
    

    if(networkConnection == 'online') {
        // if online, then sync must have happened first, no reason to not have code
        $.post( `${baseURL}/login`, {
            username: "admin", password: "1234"
        }).done(async function(data) {
            // store sessionId and add location
            var session = data.SessionId;

            await $.ajax({
                type: "PUT",
                url: `${baseURL}/api/location/update/${code.trim()}`,
                data: { 
                    U_ADDRESS: address,
                    U_UPDATED_AT: moment().format()
                },
                headers: {
                    Authorization: `B1SESSION=${session}`
                },
                dataType: 'json',
                success: function() {
                    console.log("Successfully updated");
                    
                },
                error: function(err) {
                    console.log("Error updating location online: ", JSON.stringify(err));
                }
            })

        }).fail(function(err){
        }).done(function(err) {
        })
    } 

    return await new Promise(function(resolve) {
        db.transaction(function(transaction) {
            transaction.executeSql("UPDATE sampletable SET U_ADDRESS = ?, U_UPDATED_AT = ? WHERE Id = ?", [address, moment().format(), id], function(err, resultSet) {
                console.log(JSON.stringify(resultSet));
                if(resultSet.rowsAffected > 0 ){
                    document.getElementById("upd_id").value = "";
                    document.getElementById("upd_code").value = "";
                    document.getElementById("upd_address").value = "";
                    alert("Successfully Updated")
                    showLocations();
                    resolve(resultSet);
                } else if(resultSet.rowsAffected == 0 ){
                    resolve(null);
                } else {
                    reject(err);
                }
            })
        })
    })
}

async function sync() {
    await onlineToOffline();
    await offlineToOnline();
}

async function onlineToOffline() {
    let offline = await getLocations();
    let withCode = [];
    let onlineToOfflineAdd = [];
    let onlineToOfflineUpdate = [];
    let offlineToOnlineUpdate = [];
    let sessionId;
    
    if(offline && offline.rows.length != 0) {
        $.post( `${baseURL}/login`, {
            username: "admin", password: "1234"
        }).done(async function(data) {
            // store sessionId and add location
            sessionId = data.SessionId;
    
            for(var i = 0; i < offline.rows.length; i++) {
                if(offline.rows.item(i).Code) {
                    withCode.push(offline.rows.item(i));
                }
            }
    
            await $.get(`${baseURL}/api/location/select`, async function(data) {
                
                online = data.view;
                if(online.length > 0) {
                    for (var i = 0; i < online.length; i++) {
                        console.log("online", JSON.stringify(online[i]));
                        // we want to know if online[i] is found in offline
                        var match = false; // we haven't found it yet
                        for (var j = 0; j < withCode.length; j++) {
                            if (online[i].Code == withCode[j].Code)  {
                                console.log("online code == with code", JSON.stringify(online[i]), JSON.stringify(withCode[i]));
                                match = true;
                                
                                if (online[i].U_ADDRESS != withCode[j].U_ADDRESS) {
                                    // we have found online code in offline code,
                                    // then we found out they dont have the same address,
                                    // so one of them has been updated later than the other
        
                                    if(online[i].U_ADDRESS.U_UPDATED_AT != null &&
                                        online[i].U_ADDRESS.U_UPDATED_AT > withCode[j].U_UPDATED_AT) {
                                            // online location is more up to date than offline location
                                            console.log(JSON.stringify(online[i]));
                                            onlineToOfflineUpdate.push(online[i]);
                                            break;
                                    } else if(online[i].U_ADDRESS.U_UPDATED_AT != null &&
                                    online[i].U_ADDRESS.U_UPDATED_AT < withCode[j].U_UPDATED_AT) {
                                        // offline location is more up to date than online location
                                        offlineToOnlineUpdate.push(withCode[j]);
                                        break;
                                        
                                    }
                                    
                                } else {
                                    break;
                                }
                            }
                        }
                        if(!match) {
                            // online code is not found in offline code
                            onlineToOfflineAdd.push(online[i]);
                        }
                    
                    }
    
                }
            })

            // console.log("onlineToOfflineAdd", JSON.stringify(onlineToOfflineAdd));
            // console.log("onlineToOfflineUpdate", JSON.stringify(onlineToOfflineUpdate));
            // console.log("offlineToOnlineUpdate",JSON.stringify(offlineToOnlineUpdate));
    
            // adding and updating
            let addingErr = []
            let addingSuccess = []
            let updateSuccess = [];
            let updateErr = [];
            if(onlineToOfflineAdd != null) {
                for(var i = 0; i < onlineToOfflineAdd.length; i++) {
                    // console.log("::::onlineToOfflineAdd", onlineToOfflineAdd[i]);
                    await db.transaction(async function(transaction) {
                        await transaction.executeSql('INSERT INTO sampletable(Code, U_ADDRESS, U_CREATED_AT, U_UPDATED_AT) VALUES (?, ?, ?, ?)', 
                        [
                            onlineToOfflineAdd[i].Code,
                            onlineToOfflineAdd[i].U_ADDRESS,
                            onlineToOfflineAdd[i].U_CREATED_AT,
                            onlineToOfflineAdd[i].U_UPDATED_AT
                        ]);
                    }, async function (error) {
                        console.log("ADDING ERROR: "+ onlineToOfflineAdd[i].Code + ' ' + error.message);
                        addingErr.push({
                            error: error,
                            data: onlineToOfflineAdd[i]
                        })
                    }, async function() {
                        addingSuccess.push(onlineToOfflineAdd[i])
                    })
                }
            }
            
    
            if(onlineToOfflineUpdate != null) {
                for(var i = 0; i < onlineToOfflineUpdate.length; i++) {
                    await db.transaction(function(transaction) {
                        transaction.executeSql("UPDATE sampletable SET U_ADDRESS = ?, U_UPDATED_AT = ? WHERE Code = ?", 
                        [
                            onlineToOfflineUpdate[i].U_ADDRESS,
                            onlineToOfflineUpdate[i].U_UPDATED_AT,
                            onlineToOfflineUpdate[i].Code
                        ])
                    }, function (error) {
                        console.log("UPDATING ERROR: " + onlineToOfflineUpdate[i].Code + ' '+ error.message);
                        // updateErr.push({
                        //     error: error,
                        //     data: onlineToOfflineUpdate[i]
                        // })
                    }, function() {
                        console.log("UPDATING SUCCESS");
                        updateSuccess.push(onlineToOfflineUpdate[i])
                    })
                }
            }
    
            if(offlineToOnlineUpdate.length) {
            }
    
        }).fail(function(err){
            console.log("Error: ", JSON.stringify(err));
        }).always(async function() {
            // alert("Download online data done!");
            await showLocations();
        })
    } else {
        // copy all data online to offline
        let addingSuccess = [];
        let addingErr = [];
        await $.get(`${baseURL}/api/location/select`, async function(data) {
                
            let online = data.view;
            if(online.length > 0) {
                for (var i = 0; i < online.length; i++) {
                    await new Promise(function(resolve) {
                        db.transaction(function(transaction) {
                            transaction.executeSql("INSERT INTO sampletable(Code, U_ADDRESS, U_CREATED_AT, U_UPDATED_AT) VALUES (?, ?, ?, ?)", [online[i].Code, online[i].U_ADDRESS, online[i].U_CREATED_AT, online[i].U_UPDATED_AT], function(err, resultSet) {
                                if(resultSet.rowsAffected > 0 ){
                                    resolve(resultSet);
                                } else if(resultSet.rowsAffected == 0 ){
                                    resolve(null);
                                } else {
                                    reject(err);
                                }
                            })
                        })
                    })
                }
                await showLocations();
            }
        })
    }
    await showLocations();
}

async function offlineToOnline() {
    let offline = await getLocations();
    let noCode = [];
    let withCode = [];
    let online = [];
    var noCodeForUpdate = [];

    console.log(JSON.stringify(offline));

    if(offline) {
        
        for(var i = 0; i < offline.rows.length; i++) {
            if(offline.rows.item(i).Code == null) {
                console.log(JSON.stringify(offline.rows.item(i)));
                noCode.push(offline.rows.item(i));
            } else  {
                withCode.push(offline.rows.item(i));
            }
        }

        await $.get(`${baseURL}/api/location/select`, async function(data) {
            online = data.view;
            if(withCode.length != 0) {
                for(var j = 0; j < withCode.length; j++) {
                    for(var c = 0; c < online.length; c++) {
                        if(withCode[j].Code == online[c].Code && withCode[j].U_ADDRESS != online[c].U_ADDRESS) {
                            // same code online and offline
                            // must check address similarity, if not similar,
                            // one of them is more up to date than the other
                            console.log("not same address: ", JSON.stringify(withCode[j]), JSON.stringify(online[c]));
                            // console.log("withCode[j].U_UPDATED_AT > online[c].U_UPDATED_AT: ", withCode[j].U_UPDATED_AT > online[c].U_UPDATED_AT);
                            // console.log("withCode[j].U_UPDATED_AT < online[c].U_UPDATED_AT: ", withCode[j].U_UPDATED_AT < online[c].U_UPDATED_AT);
                            if(withCode[j].U_UPDATED_AT > online[c].U_UPDATED_AT) {
                                // console.log("update online using offline data");
                                await $.post( `${baseURL}/login`, {
                                    username: "admin", password: "1234"
                                }).done(async function(data) {
                                    // store sessionId and add location
                                    var session = data.SessionId;
                        
                                    await $.ajax({
                                        type: "PUT",
                                        url: `${baseURL}/api/location/update/${withCode[j].Code}`,
                                        data: { 
                                            U_ADDRESS: withCode[j].U_ADDRESS,
                                        },
                                        headers: {
                                            Authorization: `B1SESSION=${session}`
                                        },
                                        dataType: 'json',
                                        success: function() {
                                            console.log("Successfully updated");
                                            
                                        },
                                        error: function(err) {
                                            console.log("Error updating location online: ", JSON.stringify(err));
                                        }
                                    })
                        
                                }).fail(function(err){
                                }).done(function() {
                                    console.log("UPDATE (online) DONE");
                                })
                            } else if(withCode[j].U_UPDATED_AT < online[c].U_UPDATED_AT) {
                                // console.log("update offline using online data", JSON.stringify(online[c]));
                                await new Promise(function(resolve) {
                                    db.transaction(function(transaction) {
                                        transaction.executeSql("UPDATE sampletable SET U_ADDRESS = ?, U_UPDATED_AT = ? WHERE Code = ?", [online[c].U_ADDRESS, online[c].U_UPDATED_AT, online[c].Code], function(err, resultSet) {
                                            if(resultSet.rowsAffected > 0 ){
                                                showLocations();
                                                resolve(resultSet);
                                            } else if(resultSet.rowsAffected == 0 ){
                                                resolve(null);
                                            } else {
                                                reject(err);
                                            }
                                        })
                                    })
                                })
                                // await db.transaction(function(transaction) {
                                //     console.log(JSON.stringify(online[c]));
                                //     transaction.executeSql("UPDATE sampletable SET U_ADDRESS = ?, U_UPDATED_AT = ? WHERE Code = ?", 
                                //     [online[c].U_ADDRESS, online[c].U_UPDATED_AT, online[c].Code])
                                // }, function (error) {
                                //     console.log("UPDATING ERROR: " + online[c].Code + ' '+ error.message);
                                // }, function() {
                                //     console.log("UPDATE SUCCESS");
                                // })
                            }
                        }
                    }
                }
            }

            
            if(online.length > 0) {
                // check if data offline has no code, but exists online
                // should update and get code from online
                for(var i = 0; i < online.length; i++) {
                    for(var j = 0; j < noCode.length; j++) {
                        if(online[i].U_ADDRESS == noCode[j].U_ADDRESS) {
                            noCodeForUpdate.push({
                                Code: online[i].Code,
                                Id: noCode[j].Id
                            })
                        }
                    }
                }

                if(noCodeForUpdate.length > 0) {
                    // update to copy Code from online to offline
                    for(var i = 0; i < noCodeForUpdate.length; i++) {
                        await new Promise(function(resolve) {
                            db.transaction(function(transaction) {
                                transaction.executeSql("UPDATE sampletable SET Code = ? WHERE Id = ?", [noCodeForUpdate[i].Code, noCodeForUpdate[i].Id], function(err, resultSet) {
                                    if(resultSet.rowsAffected > 0 ){
                                        showLocations();
                                        resolve(resultSet);
                                    } else if(resultSet.rowsAffected == 0 ){
                                        resolve(null);
                                    } else {
                                        reject(err);
                                    }
                                })
                            })
                        })
                    }
                }
            }
        })

        var forAdd = [];
        if(noCode.length > 0 && noCodeForUpdate.length > 0) {
            for(var i = 0; i < noCode.length; i++) {
                for(var j = 0; j < noCodeForUpdate.length; i++) {
                    if(noCode[i].Id == noCodeForUpdate[j].Id) {
                        forAdd.push(noCode[i]);
                    }
                }
            }

            if(forAdd.length > 0) {
                await $.post( `${baseURL}/login`, {
                    username: "admin", password: "1234"
                }).done(async function(data) {
                    // store sessionId and add location
                    var session = data.SessionId;
                    for(var a = 0; a < forAdd.length; a++) {
                        console.log(forAdd[a].U_ADDRESS);
                        await $.ajax({
                            type: "POST",
                            url: `${baseURL}/api/location/add`,
                            data: { 
                                U_ADDRESS: forAdd[a].U_ADDRESS,
                            },
                            headers: {
                                Authorization: `B1SESSION=${session}`
                            },
                            dataType: 'json',
                            success: function() {
                                console.log("Successfully added");
                            },
                            error: async function(err) {
                                console.log("Error adding location online: ", JSON.stringify(err));
                            }
                        })
                    }
                }).fail(function(err){
                    console.log("err: ", JSON.stringify(err));
                }).always(async function() {
                    await showLocations();
                })
            }
        } else if(noCode.length > 0) {
            await $.post( `${baseURL}/login`, {
                username: "admin", password: "1234"
            }).done(async function(data) {
                // store sessionId and add location
                var session = data.SessionId;
                for(var a = 0; a < noCode.length; a++) {
                    console.log(noCode[a].U_ADDRESS);
                    await $.ajax({
                        type: "POST",
                        url: `${baseURL}/api/location/add`,
                        data: { 
                            U_ADDRESS: noCode[a].U_ADDRESS,
                        },
                        headers: {
                            Authorization: `B1SESSION=${session}`
                        },
                        dataType: 'json',
                        success: function() {
                            console.log("Successfully added");
                        },
                        error: async function(err) {
                            console.log("Error adding location online: ", JSON.stringify(err));
                            console.log(err.responseText.includes("already exists"));
                        }
                    })
                }
            }).fail(function(err){
                console.log("err: ", JSON.stringify(err));
            }).always(async function() {
                await showLocations();
            })
        }
    }
    await showLocations();
}