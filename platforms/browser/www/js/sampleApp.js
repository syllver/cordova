async function submitMe() {
    var firstname =  document.getElementById("firstname").value;
    var age = document.getElementById("age").value;
    console.log("firstname and age:", firstname + ": " + age);

    if(firstname != "" && age != "") {
        await insertInfo();
        document.getElementById("firstname").value = ""
        document.getElementById("firstname").innerText = ""
        document.getElementById("age").innerText = ""
        document.getElementById("age").value = ""
    }
}

function showData(arr) {
    var table = document.getElementById("table");
    table.innerHTML = '';

    for(var i = 0; i < arr.length; i++) {  
        var row = table.insertRow(0);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        // var cell5 = row.insertCell(4);
        cell1.innerText = arr[i].id;
        cell2.innerText = arr[i].name;
        cell3.innerText = arr[i].age;
        cell4.innerText = new Date(arr[i].date_created).toLocaleDateString('en-GB', {
            day : 'numeric',
            month : 'short',
            year : 'numeric',
            hour12: false,
            hour: '2-digit',
            minute: '2-digit'
        });
    //     cell4.innerHTML = `<button class="btn btn-sm btn-primary" 
    //         onclick="updateMe(${arr[i].id})"
    //     >Edit</button>`;
    //     cell5.innerHTML = `<button class="btn btn-sm btn-danger" 
    //         onclick="deleteInfo(${arr[i].id})"
    //     >Delete</button>`;
    }
}

async function updateMe(id) {
    await getInfoById(id).then(res => {
        var updateModal = new bootstrap.Modal(document.getElementById('updateModal'));
        updateModal.show();
    })
    
}

function cancelUpdate() {
    document.getElementById("upd_firstname").value = "";
    document.getElementById("upd_age").value = "";
    document.getElementById("upd_id").value = "";
}
