
Requirements need to run
# Cordova
# Android Studio, JDK 1.8.0
# Gradle (>=7.0.2)

```
# Build Setup
# install cordova
$ npm install -g cordova

# install android studio with gradle
# clone repository
# cd to repository
$ cd cordova

# install dependencies
$ npm install

# generate static project
$ cordova emulate <platform>

# platform can be either android or browser
$ cordova emulate android
$ cordova emulate browser

```

# Usage
```
This app is created using Apache Cordova with the following
Technologies: HTML, CSS, JavaScript
Dependencies: Jquery (AJAX)

The local database used is SQLite. The mobile app is also connected online to the API of FDSS-EUT.

This sample app can be used to add a location for FDSS printers.
If offline, the data will be saved locally. Once the device detects an online network, data can be synced online and offline.
```